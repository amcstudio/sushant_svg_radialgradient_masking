'use strict';
~ (function() {
    var tl,
    maskCircle = document.getElementById('maskCircle'),
    ad = document.getElementById('mainContent');

    window.init = function() {
        setTimeout(play, 1000);
    }

    function play() {
        tl = gsap.timeline(); 

        tl.addLabel('frameOne')
        tl.to(maskCircle, 1, {attr:{r:200}, ease:'power1Out'},'frameOne');
        tl.to('#whiteMask', 1, {attr:{offset:'100%'}, ease:'power1Out'},'frameOne');
        
        tl.to(maskCircleTwo, 1, {attr:{r:200}, ease:'power1Out'},'frameOne+=2');
        tl.to('#whiteMaskTwo', 1, {attr:{offset:'100%'}, ease:'power1Out'},'frameOne+=2');
    }

}) ();
